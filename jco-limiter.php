<?php

/**
 * JCO rate-limiting plugin
 *
 * @package   JCO\Limiter
 * @copyright Copyright (c) 2021 JCO Digital.
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: JCOre Limiter
 * Plugin URI:  https://bitbucket.org/jcodigital/jco-limiter
 * Description: Rate limit endpoints in the WP-Rest API.
 * Version:     1.1.2
 * Author:      JCO Digital
 * Author URI:  https://jco.fi
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require __DIR__ . '/classes/Limiter.php';

use RateLimiter\Limiter;

/**
 * Singleton access to the current limiter instance
 */
function limiter_instance() {
	static $instance;
	if ( null === $instance ) {
		$instance = new Limiter();
	}
	return $instance;
}

$jco_limiter = limiter_instance();

add_action(
	'rest_api_init',
	static function () use ( $jco_limiter ) {
		$jco_limiter->load();
	}
);
