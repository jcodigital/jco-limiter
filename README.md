# JCO Rate Limiter

This is a plugin for restricting access to a/many WP REST API Endpoint.

Example for enabling rate limiting.

```
register_rest_route(
		'v1/rate-limited',
		'/rate-limited',
		array(
			'methods'             => 'GET',
			'callback'            => 'rate_limit_me',
			'permission_callback' => '__return_true',
			'rate_limiter'        => array(
				'uses'     => 5,
				'interval' => 120,
			),
		)
	);
```

Adding the rate_limiter array will enable ratelimiting.
