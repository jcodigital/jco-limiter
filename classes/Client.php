<?php


namespace RateLimiter;

class Client {

	/**
	 * The client id.
	 *
	 * @var string|int
	 */
	private string|int $id;

	/**
	 * The interval (in seconds) before the usage timer is reset.
	 *
	 * @var int
	 */
	private int $interval;

	/**
	 * Maximum allowed uses during an interval.
	 *
	 * @var int
	 */
	private int $max_uses;

	/**
	 * The amount of uses during a interval.
	 *
	 * @var int
	 */
	private int $uses;

	/**
	 * Start time of an interval.
	 *
	 * @var int
	 */
	private int $start;


	/**
	 * Client constructor.
	 *
	 * @param int|string $id The id of the client.
	 * @param int $max_uses The max uses for the client.
	 * @param int $interval The interval for the client.
	 */
	public function __construct( int|string $id, int $max_uses, int $interval ) {
		$this->id       = $id;
		$this->max_uses = $max_uses;
		$this->interval = $interval;
		$this->uses     = $max_uses;
		$this->start    = time();
	}

	/**
	 * Adds a number of uses to this client.
	 *
	 * @param int $times The amount of uses to add.
	 *
	 * @return Client
	 */
	final public function add_use( int $times = 1 ): static {
		$this->uses -= $times;
		return $this;
	}

	/**
	 * Returns the clients id.
	 *
	 * @return int|string
	 */
	final public function get_id(): int|string {
		return $this->id;
	}

	/**
	 * Returns the interval for this client.
	 *
	 * @return int
	 */
	final public function get_interval(): int {
		return $this->interval;
	}

	/**
	 * Returns the max uses for the client.
	 *
	 * @return int
	 */
	final public function get_max_uses(): int {
		return $this->max_uses;
	}

	/**
	 * Returns the clients current uses during this interval.
	 *
	 * @return int
	 */
	final public function get_uses(): int {
		return $this->uses;
	}

	/**
	 * Returns the start time for this interval.
	 *
	 * @return int
	 */
	final public function get_start(): int {
		return $this->start;
	}

	/**
	 * Returns when this interval will reset.
	 *
	 * @return int
	 */
	final public function get_reset(): int {
		return $this->start + $this->interval - time();
	}

	/**
	 * Whether the max uses has been exceeded.
	 *
	 * @return bool
	 */
	final public function is_max_uses_exceeded(): bool {
		return 0 > $this->get_uses();
	}

	/**
	 * Retrieve rate limit headers.
	 *
	 * @return array
	 */
	final public function get_headers(): array {
		$headers = array(
			'X-RateLimit-Limit'     => $this->get_max_uses(),
			'X-RateLimit-Remaining' => $this->get_uses(),
			'X-RateLimit-Reset'     => $this->get_reset(),
		);

		if ( $this->is_max_uses_exceeded() ) {
			$headers['Retry-After']           = $this->get_reset();
			$headers['X-RateLimit-Remaining'] = 0;
		}

		return $headers;
	}

	/**
	 * Converts the client to an array.
	 *
	 * @return array
	 */
	final public function to_array(): array {
		return array(
			'max_uses'  => $this->get_max_uses(),
			'remaining' => $this->is_max_uses_exceeded() ? 0 : $this->get_max_uses(),
			'reset'     => $this->get_reset(),
		);
	}
}
