<?php

namespace RateLimiter;

use WP_REST_Request;
use WP_REST_Response;

require_once 'Client.php';

/**
 * Class Limiter handles the limiting of rest endpoints.
 *
 * @package RateLimiter
 */
class Limiter {

	/**
	 * Limiter constructor.
	 * Does nothing.
	 */
	public function __construct() {}

	/**
	 * Returns an identifier to be used when throttling requests.
	 * This can either be the currently logged in users id or the requests IP.
	 *
	 * @return string|int
	 */
	private function identify() {
		if ( is_user_logged_in() ) {
			return get_current_user_id();
		}

		return $_SERVER['REMOTE_ADDR'];
	}

	/**
	 * Builds the cache key for a request.
	 *
	 * @param WP_REST_Request $request
	 * @return string
	 */
	private function build_cache_key( WP_REST_Request $request ): string {
		return sprintf( 'JCOre:RateLimiter:%s%d', filter_var( $request->get_route(), FILTER_SANITIZE_ENCODED ), $this->identify() );
	}

	/**
	 * Loads the rate-limiters filters.
	 *
	 * @return void
	 */
	final public function load(): void {
		add_filter( 'rest_dispatch_request', array( $this, 'throttle' ), 10, 3 );
	}


	/**
	 * Handles the throttling of enabled endpoints.
	 *
	 * @param mixed           $response The WP_REST_Response object.
	 * @param WP_REST_Request $request The WP_REST_Request object.
	 *
	 * @return mixed The modified response object or nothing, if the request is not rate limited.
	 */
	final public function throttle( mixed $response, WP_REST_Request $request ): mixed {
		$request_attributes = $request->get_attributes();

		$rate_enabled = isset( $request_attributes['rate_limiter'] );

		// Exit if the current route is not enabled.
		if ( ! $rate_enabled || true === apply_filters( 'jco_rate_limiter_skip', WP_DEBUG ) ) {
			return $response;
		}

		$max_uses = $request_attributes['rate_limiter']['uses'] ?? 1;
		$interval = $request_attributes['rate_limiter']['interval'] ?? 60;

		$client = $this->create_client( $request, $max_uses, $interval );

		if ( 'HEAD' !== $request->get_method() ) {
			$client->add_use();
		}

		if ( $client->is_max_uses_exceeded() ) {
			$data = array(
				'error'   => 'rate_limit_exceeded',
				'message' => 'Too many requests reached',
				'data'    => array_merge(
					$client->to_array(),
					array( 'status' => 429 )
				),
			);

			$response = new WP_REST_Response( $data, 429 );
		}

		$this->save_client( $request, $client );

		return $response;
	}

	/**
	 * Creates a client from cache or creates a new client.
	 *
	 * @param WP_REST_Request $request The WP_REST_Request object.
	 * @param int             $max_uses Max uses for the client.
	 * @param int             $interval Interval for the client uses.
	 *
	 * @return Client
	 */
	private function create_client( WP_REST_Request $request, int $max_uses, int $interval ) {
		$client = get_transient( $this->build_cache_key( $request ) );

		if ( false === $client ) {
			$client = new Client( $this->identify(), $max_uses, $interval );
		}

		return $client;

	}

	/**
	 * Saves the client in the cache.
	 *
	 * @param WP_REST_Request $request The WP_REST_Request object.
	 * @param Client          $client The client object.
	 *
	 * @return void
	 */
	private function save_client( WP_REST_Request $request, Client $client ) {
		set_transient(
			$this->build_cache_key( $request ),
			$client,
			$client->get_reset()
		);
	}

}
